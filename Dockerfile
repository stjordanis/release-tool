#
# THORChain release tool
#

#
# Build
#
FROM golang:1.14.7 AS build

ENV GOBIN=/go/bin
ENV GOPATH=/go
ENV CGO_ENABLED=0
ENV GOOS=linux

WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download

COPY . .
RUN go build
#
# Main
#
FROM alpine

RUN apk add --update jq curl bind-tools && \
    rm -rf /var/cache/apk/*

# Copy the compiled binaires over.
COPY --from=build /app/release-tool /usr/bin/

ENTRYPOINT ["release-tool","changelog"]