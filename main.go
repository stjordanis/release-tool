package main

import (
	"log"
	"os"

	"github.com/urfave/cli/v2"
)

func main() {

	app := &cli.App{
		Name:  "release-tool",
		Usage: "",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "token",
				Usage:       "personal access token use to access gitlab",
				EnvVars:     []string{"CI_ACCESS_TOKEN"},
				Required:    true,
				Hidden:      false,
				Value:       "",
				DefaultText: "",
				HasBeenSet:  false,
			},
		},
		Commands: cli.Commands{
			getChangeLogCmd(),
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
