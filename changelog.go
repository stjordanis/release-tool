package main

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/blang/semver"
	"github.com/urfave/cli/v2"
	"github.com/xanzy/go-gitlab"
	"gopkg.in/yaml.v2"
)

const (
	changelogPrefix = "changelogs/unreleased"
	changelog       = "changelogs/CHANGELOG.md"
	versionFile     = "version"
)

var (
	all_types = [...]string{
		"security", "fixed", "added", "changed", "deprecated", "removed", "performance", "other",
	}
)

type ChangeLogItem struct {
	Title        string `yaml:"title"`
	MergeRequest string `yaml:"merge_request"`
	Author       string `yaml:"author"`
	Type         string `yaml:"type"`
}

func getChangeLogCmd() *cli.Command {
	return &cli.Command{
		Name:        "changelog",
		Aliases:     []string{"cl"},
		Usage:       "update CHANGELOG",
		UsageText:   "",
		Description: "",
		ArgsUsage:   "",
		Category:    "",
		Action:      updateChangeLog,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "pid",
				Usage:       "project id",
				EnvVars:     []string{"CI_PROJECT_ID"},
				Required:    true,
				Hidden:      false,
				Value:       "",
				DefaultText: "project id that can uniquely identify the project ",
				HasBeenSet:  false,
			},
			&cli.StringFlag{
				Name:        "sha",
				Usage:       "commit hash",
				EnvVars:     []string{"CI_COMMIT_SHA"},
				Required:    true,
				Hidden:      false,
				Value:       "",
				DefaultText: "project id that can uniquely identify the project ",
				HasBeenSet:  false,
			},
			&cli.StringFlag{
				Name:       "branch",
				Usage:      "target branch",
				EnvVars:    []string{"CI_COMMIT_BRANCH"},
				Required:   false,
				Hidden:     false,
				Value:      "master",
				HasBeenSet: false,
			},
			&cli.BoolFlag{
				Name:       "update",
				Usage:      "update changelogs/CHANGELOG.md file or not",
				Required:   false,
				Hidden:     false,
				HasBeenSet: false,
			},
		},
	}
}

func updateChangeLog(ctx *cli.Context) error {
	projectID := ctx.String("pid")
	sha := ctx.String("sha")
	branch := ctx.String("branch")
	versionChanged, err := hasVersionChanged(ctx, projectID, sha)
	if err != nil {
		return fmt.Errorf("fail to check whether version changed or not: %w", err)
	}
	if !versionChanged {
		fmt.Println("version has not been updated , no need to update change log and release..")
		return nil
	}

	version, lastCommitID, err := getVersion(ctx, projectID, sha)
	if err != nil {
		return fmt.Errorf("fail to get version file on branch(%s): %w", branch, err)
	}
	fmt.Println("current version:", version)
	commitIDBeforeMerge, err := getFileCommits(ctx, projectID, lastCommitID)
	if err != nil {
		return fmt.Errorf("fail to get the commit id before merge: %w", err)
	}

	mrHash, err := getMergeRequestHash(ctx, projectID, commitIDBeforeMerge, branch)
	if err != nil {
		fmt.Printf("fail to get the merge request hash before current: %s", err)
		mrHash = commitIDBeforeMerge
	}
	fmt.Println("last merge request hash: ", mrHash)
	fmt.Println("previous merge request hash:", commitIDBeforeMerge)
	files, err := getChangeLogFiles(ctx, projectID, mrHash, sha)
	if err != nil {
		return fmt.Errorf("fail to get change log files: %w", err)
	}
	if len(files) == 0 {
		fmt.Println("didn't find any change log files")
		return nil
	}
	changeItems, err := getChangeLogItems(ctx, projectID, files, branch)
	if err != nil {
		return fmt.Errorf("fail to get change log items: %w", err)
	}
	content := getReleaseContent(ctx, projectID, version.String(), changeItems)
	if ctx.IsSet("update") && ctx.Bool("update") {
		if err := appendChangeLog(ctx, projectID, branch, content); err != nil {
			return fmt.Errorf("fail to update change log: %w", err)
		}
	}
	fmt.Println(content)
	if err := createTagAndRelease(ctx, projectID, version.String(), branch, content); err != nil {
		return fmt.Errorf("fail to create tag and release: %w", err)
	}
	return nil
}

func createTagAndRelease(ctx *cli.Context, projectID, version, branch, content string) error {
	client := getGitlabClient(ctx)
	tagName := fmt.Sprintf("v%s", version)
	if branch != "master" {
		tagName += "-" + branch
	}
	t, _, err := client.Tags.CreateTag(projectID, &gitlab.CreateTagOptions{
		TagName:            gitlab.String(tagName),
		Ref:                gitlab.String(branch),
		Message:            gitlab.String("release " + tagName),
		ReleaseDescription: gitlab.String(content),
	})
	if err != nil {
		return fmt.Errorf("fail to create tag v%s:%w", versionFile, err)
	}
	fmt.Println(t.String())

	return nil
}
func appendChangeLog(ctx *cli.Context, projectID, branch, content string) error {
	client := getGitlabClient(ctx)
	cl, err := getFileContent(ctx, projectID, changelog, branch)
	if err != nil {
		return fmt.Errorf("fail to get change log: %w", err)
	}
	buf := bytes.Buffer{}
	if _, err := buf.WriteString(content); err != nil {
		return fmt.Errorf("fail to write content to buf: %w", err)
	}

	if _, err := buf.Write(cl); err != nil {
		return fmt.Errorf("fail to write content to buf: %w", err)
	}

	fi, _, err := client.RepositoryFiles.UpdateFile(projectID, changelog, &gitlab.UpdateFileOptions{
		Branch:        gitlab.String(branch),
		Content:       gitlab.String(buf.String()),
		CommitMessage: gitlab.String("[bot] update changelog"),
	})
	if err != nil {
		return fmt.Errorf("fail to update change log file: %w", err)
	}
	fmt.Println(fi)
	return nil
}

func getGitlabClient(ctx *cli.Context) *gitlab.Client {
	token := ctx.String("token")
	client, err := gitlab.NewClient(token)
	if err != nil {
		panic(err)
	}
	return client
}

func hasVersionChanged(ctx *cli.Context, projectID string, sha string) (bool, error) {
	client := getGitlabClient(ctx)
	diffs, resp, err := client.Commits.GetCommitDiff(projectID, sha, nil)
	if err != nil {
		return false, fmt.Errorf("fail to get commit diff: %w", err)
	}
	if resp.StatusCode != http.StatusOK {
		return false, fmt.Errorf("fail to get commit diff, unexpected status: %s", resp.Status)
	}
	for _, d := range diffs {
		if strings.EqualFold(d.NewPath, versionFile) {
			return true, nil
		}
	}
	return false, nil
}

func getChangeLogFiles(ctx *cli.Context, projectID, lastHash, currentHash string) ([]string, error) {
	client := getGitlabClient(ctx)
	compare, resp, err := client.Repositories.Compare(projectID, &gitlab.CompareOptions{
		From:     gitlab.String(lastHash),
		To:       gitlab.String(currentHash),
		Straight: gitlab.Bool(true),
	})
	if err != nil {
		return nil, fmt.Errorf("fail to get change log item: %w", err)
	}
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("fail to get commit diff, unexpected status: %s", resp.Status)
	}
	var changeLogFiles []string
	for _, item := range compare.Diffs {
		// file need to be in changelogs/unreleased folder
		if !item.DeletedFile && item.NewFile && strings.HasPrefix(item.NewPath, changelogPrefix) {
			changeLogFiles = append(changeLogFiles, item.NewPath)
		}
	}
	return changeLogFiles, nil
}

func getReleaseContent(ctx *cli.Context, projectID, version string, changeLogItems []ChangeLogItem) string {
	content := strings.Builder{}
	content.WriteString(fmt.Sprintf("# %s (%s)\n", version, time.Now().Format("2006-01-02")))
	for _, t := range all_types {
		changeItemsByType := getChangeLogItemByType(changeLogItems, t)
		if len(changeItemsByType) == 0 {
			continue
		}
		content.WriteString(fmt.Sprintf("## %s\n", t))
		var listItem string
		for _, item := range changeItemsByType {
			listItem = "* " + item.Title
			if item.Author != "" {
				listItem += "(" + item.Author + ")"
			}
			if item.MergeRequest != "" {
				url, err := getMergeRequestURL(ctx, projectID, item.MergeRequest)
				if err != nil {
					fmt.Printf("fail to get merge request url: %s \n", err)
				} else {
					listItem += " ( " + url + ")"
				}
			}
			content.WriteString(listItem + "\n")
		}
	}
	return content.String()
}

func getMergeRequestURL(ctx *cli.Context, projectID, mergeRequestID string) (string, error) {
	client := getGitlabClient(ctx)
	mrID, err := strconv.Atoi(mergeRequestID)
	if err != nil {
		return "", fmt.Errorf("fail to convert merge request id(%s) to int", mergeRequestID)
	}
	mergeRequest, resp, err := client.MergeRequests.GetMergeRequest(projectID, mrID, nil)
	if err != nil {
		return "", fmt.Errorf("fail to get merge request: %w", err)
	}
	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("unexpected response status: %s", resp.Status)
	}
	return mergeRequest.WebURL, nil
}

func getChangeLogItemByType(items []ChangeLogItem, changeType string) []ChangeLogItem {
	var result []ChangeLogItem
	for _, item := range items {
		if strings.EqualFold(item.Type, changeType) {
			result = append(result, item)
		}
	}
	return result
}

func getChangeLogItems(ctx *cli.Context, projectID string, files []string, branch string) ([]ChangeLogItem, error) {
	var changeLogItems []ChangeLogItem
	for _, item := range files {
		content, err := getFileContent(ctx, projectID, item, branch)
		if err != nil {
			return nil, fmt.Errorf("fail to get file content(%s): %w", item, err)
		}
		var logItem ChangeLogItem
		if err := yaml.Unmarshal(content, &logItem); err != nil {
			return nil, fmt.Errorf("fail to unmarshal change log item(%s): %w", item, err)
		}
		changeLogItems = append(changeLogItems, logItem)
	}
	return changeLogItems, nil
}

func getFileContent(ctx *cli.Context, projectID, filePathName, branch string) ([]byte, error) {
	client := getGitlabClient(ctx)
	f, resp, err := client.RepositoryFiles.GetFile(projectID, filePathName, &gitlab.GetFileOptions{
		Ref: gitlab.String(branch),
	})
	if err != nil {
		return nil, fmt.Errorf("fail to get file: %w", err)
	}
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected response status: %s", resp.Status)
	}
	content, err := base64.StdEncoding.DecodeString(f.Content)
	if err != nil {
		return nil, fmt.Errorf("fail to decode version: %w", err)
	}
	return content, nil
}

func getVersion(ctx *cli.Context, projectID, commitHash string) (semver.Version, string, error) {
	client := getGitlabClient(ctx)
	f, resp, err := client.RepositoryFiles.GetFile(projectID, versionFile, &gitlab.GetFileOptions{
		Ref: gitlab.String(commitHash),
	})
	if err != nil {
		return semver.Version{}, "", fmt.Errorf("fail to get version(%s): %w", commitHash, err)
	}
	if resp.StatusCode != http.StatusOK {
		return semver.Version{}, "", fmt.Errorf("unexpected response status: %s", resp.Status)
	}
	content, err := base64.StdEncoding.DecodeString(f.Content)
	if err != nil {
		return semver.Version{}, "", fmt.Errorf("fail to decode version: %w", err)
	}
	content = bytes.TrimSuffix(content, []byte{'\n'})
	return semver.MustParse(string(content)), f.LastCommitID, nil
}

func getFileCommits(ctx *cli.Context, projectID, hashID string) (string, error) {
	client := getGitlabClient(ctx)
	commits, resp, err := client.Commits.ListCommits(projectID, &gitlab.ListCommitsOptions{
		Path: gitlab.String(versionFile),
	})
	if err != nil {
		return "", fmt.Errorf("fail to get commit history: %w", err)
	}
	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("unexpected response status: %s", resp.Status)
	}
	// return the first commit that is before this
	for _, commit := range commits {
		if commit.ID != hashID {
			return commit.ID, nil
		}
	}
	return "", fmt.Errorf("didn't find")
}
func getMergeRequestHash(ctx *cli.Context, projectID, hashID, targetBranch string) (string, error) {
	client := getGitlabClient(ctx)
	mrs, resp, err := client.Commits.GetMergeRequestsByCommit(projectID, hashID)
	if err != nil {
		return "", fmt.Errorf("fail to get merge request from commit: %w", err)
	}
	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("unexpected response status: %s", resp.Status)
	}
	for _, mr := range mrs {
		if mr.TargetBranch == targetBranch {
			return mr.MergeCommitSHA, nil
		}
	}

	return "", fmt.Errorf("didn't find merge request")
}
