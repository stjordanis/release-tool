module gitlab.com/thorchain/release-tool

go 1.15

require (
	github.com/blang/semver v3.5.1+incompatible
	github.com/urfave/cli/v2 v2.2.0
	github.com/xanzy/go-gitlab v0.37.0
	gopkg.in/yaml.v2 v2.2.2
)
